$(document).ready(function(){
	
	$("#searchBTn").on("click",function(){
		hasilCari()
	})
	
	$("#clear").on("click",function(){
		$("#katagoriID").val("");
		$("#kataKunciID").val("");
		$("#cekObat1").prop("checked",true);
		$("#cekObat2").prop("checked",false);
		$("#hargaAwalID").val("");
		$("#hargaAkhirlID").val("");
	});
	dropdownSelect();
})

function hasilCari(){
		var param=[];
		param[0]=$("#katagoriID").val();
		param[1]=$("#kataKunciID").val();
		param[2]=$("input[name='exampleRadios']:checked").val();
		console.log(param);
		$.ajax({
			url		:"http://localhost:8080/api/cariObat/hasilCari/"+param,
			type	:"GET",
			contentType	:"application/json",
			success	:function(data){
				console.log(data);
				var cek = "";
				if (param[2] = 1) {
					cek = "Cari semua obat(Termasuk obat keras)";
				}else{
					cek = "Hanya cari obat bebas tanpa resep";
				}
				var str = "<div class='card'>" + "<div class='card-header'>Hasil Cari Obat</div>" 
				str += "<div class='card-body'>" + "<div class='container'>" + "<div class='row'>" + "<div class='col-8'>Hasil pencarian berdasarkan kata kunci" +
						": katagori "+param[0]+ " , " + " kata kunci " +param[1] + " , " + cek +"</div>";
				str+= "<div class='col-4'><button class='btn btn-outline-primary' onclick='ulangiCariObat()' >ulangi pencarian</button></div>";
				for (var i = 0; i < data.length; i++) {
					str+= "<div class='col-md-5'>"
					str+="<div class='card' style='margin-top:25px; width: 25rem'>" + "<div class='text-center'>" + "<img class='card-img-top' style='height: 50%!important; width: 50%!important; ' src="+data[i].gambar+" alt='Card image cap'></div>";
					str+=" <div class='card-body'>" + "<p class='card-title'>"+data[i].katakunci+"</p>" + " <p class='card-text'>"+data[i].komposisi+"</p>";
					str+="<button href='"+data[i].id+"' onclick='tambahKeranjang("+data[i].id+")' class='btn btn-primary btn-tambah-keranjang'>Tambah ke keranjang</button>"
					str+="<div class='form-group' style='display:none;' href='"+data[i].id+"'>";
					str+="<span><button id='kurang' class='btn btn-sm  bootstrap-touchspin-down' onclick='kurang("+data[i].id+")'>-</button></span>"
					str+="<input href='"+data[i].id+"' c id='demo0' style='border:none; border-color=transparent; display: block; margin-left: auto; margin-right: auto;' type='text' value='55' name='demo0' data-bts-min='0' data-bts-max='100' data-bts-init-val='' data-bts-step='1' data-bts-decimal='0' data-bts-step-interval='100' data-bts-force-step-divisibility='round' data-bts-step-interval-delay='500' data-bts-prefix='' data-bts-postfix='' data-bts-prefix-extra-class='' data-bts-postfix-extra-class='' data-bts-booster='true' data-bts-boostat='10' data-bts-max-boosted-step='false' data-bts-mousewheel='true' data-bts-button-down-class='btn btn-default' data-bts-button-up-class='btn btn-default'/>"
					str+="<span><button id='tambah' class='btn btn-sm bootstrap-touchspin-up text-area-center' onclick='tambah("+data[i].id+")'>+</button></span>"
					str+="</div></div></div></div>"
				}  
				    
				    
				str+= "</div></div></div></div>";
				str+="<div class='card-footer text-muted'>"+"<div class='container'>"+"<div class='row'>"+"<div class='col-8'>"
				str+="<p>1 Produk | Estimasi Harga: Rp 48.000</p></div>" + "<div class='col-4'>"+"<button class='btn btn-primary btn-keranjang'>Keranjang Saya</button></div>"
				str+="</div></div></div>"	
				$("#fragment").html(str);
				$("#modalObat").hide();
			}
		})
	}

function dropdownSelect(){
	$.ajax({
		url			: "http://localhost:8080/api/cariObat/selectAllKatagori",
		type		: "GET",
		dataType	: "JSON",
		success		: function(hasil){
			var str = "<select id='katagoriID' class='browser-default custom-select'>";
			var value = "";
			var select = "select"
				str+= "<option value = " + value +">"+ select + "</option>"	
				for (var i = 0; i < hasil.length; i++) {
					str+="<option value="+hasil[i].name+">"+hasil[i].name+"</option>"
				}
			str+="</select>"
			$(".tempKatagori").empty()
			$(".tempKatagori").append(str)
		}
	})	
}

function ulangiCariObat(){
	
	$.ajax({
		url: "/cariObat",
		type : "get",
		dataType : "html",
		success : function(result){
			$("#searchMed").html(result);

			$("#modalObat").modal("show");
		}
	})
}


	
