$(document).ready(function(){			
	pembayaranPage();
	$(document).on('click','#tambahKartu',function() {
		var str ="<div>";
		str+="<div class='form-group d-flex justify-content-between'>";
		str+="<div>";
		str+="<h1 class='card-title'>Tambah Kartu Kredit / Debit</h1x`>";
		str+="</div>";
		str+="<div>";
		str+="<button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span>";
		str+="</button></div></div>";
		str+="<div>";
		str+="<div class='form-group'>";
		str+="<label class='control-label text-muted'>No Kartu Kredit / Debit* </label>";
		str+="<input class='form-control' type='text' id='cardNumber' placeholder='contoh: xxx-xxxx-xxxx-xxxx' style='width: 100%'></div>"
		str+="<small id='inputHelp' class='form-text text-muted' style='margin-top:-15px;'>Tanda (*) wajib diisi.</small>";
		str+="<div class='d-flex flex-row bd-highlight mb-3' style='margin-top:-5px;'>"
		str+="<div class='p-2 bd-highlight'><i  class='fab fa-2x fa-cc-mastercard'></i></div>";
		str+="<div class='p-2 bd-highlight'><i class='fab fa-2x fa-cc-visa'></i></div>";
		str+="<div class='p-2 bd-highlight'><i class='fab fa-2x fa-cc-jcb'></i></div>";
		str+="<div class='p-2 bd-highlight'><i class='fab fa-2x fa-cc-amex'></i></div>";
		str+="</div>";
		str+="<div class='d-flex justify-content-around' style='margin-top:-15px;'>";
		str+="<div><label class=''>Masa Berlaku*</label>";
		str+="<p><input class='form-control' style='width:70%;' type='text' id='masaBerlaku' placeholder='yy/mm'></P></div>"
		str+="<div><label class=''>CVV*</label>";
		str+="<p><input class='form-control'style='width:70%;' type='text' id='cvv' placeholder='contoh: 123' maxlength='3'></p></div>"
		str+="</div></div>";
		$("#tambahPembayaranKartu").empty();
		$("#tambahPembayaranKartu").append(str);
		$("#pembayaran").modal("show");
	});
	
	$(document).on('click','#TambahKartu',function() {
		var tanggal = $("#masaBerlaku").val();
		if (tanggal == "") {
			tanggal = "";
		}else {
			tanggal+="/01";
			var currentYear = (new Date).getFullYear();
			var temp = String(currentYear).substring(0,2);
			tanggal = temp + tanggal;
			tanggal = tanggal.replace("/","-").replace("/","-");
		}
		
		var tempCVV = $("#cvv").val();
		var obj = {};
		obj.cardNumber = $("#cardNumber").val();
		obj.validityPeriod = tanggal;
		obj.tempCVV = tempCVV;
		obj.createdBy = 1;
		localStorage.getItem("updateId")
		var myJson = JSON.stringify(obj);
		$.ajax({
			url			:"http://localhost:8080/api/pembayaran/tambahDaftarKartu",
			type		:"POST",
			contentType	:"application/json",
//			dataType	: "json", 
			data		: myJson,
			success	: function(data){
				console.log(data);
				var title = "";
				var body = "";
				var result = data.split(",");
				if (result[0] == "kosong") {
					title = "Failed";
					body = "<p>Lengkapi kolom " +result[1]+ "</p>";
					$("#pembayaran").modal("hide");
					$(".modal-title-alert").empty();
					$(".modal-title-alert").append(title);
					$("#isialert").empty();
					$("#isialert").append(body);
					$("#alert").modal({ backdrop: 'static', keyboard: false });
					$('#alert').on('shown.bs.modal', function () {

					})
					$("#pembayaran").modal("hide");
				}if (result[0] == "full") {
					title = "Failed";
					body = "<p>"+result[1]+" kartu mencapai batas maksimal</p>";
					$("#pembayaran").modal("hide");
					$(".modal-title-alert").empty();
					$(".modal-title-alert").append(title);
					$("#isialert").empty();
					$("#isialert").append(body);
					$("#alert").modal({ backdrop: 'static', keyboard: false });
					$('#alert').on('shown.bs.modal', function () {

					})
				}if (result[0] == "ada") {
					title = "Failed";
					body = "<p>Data "+result[1]+" sudah ada!</p>";
					$("#pembayaran").modal("hide");
					$(".modal-title-alert").empty();
					$(".modal-title-alert").append(title);
					$("#isialert").empty();
					$("#isialert").append(body);
					$("#alert").modal({ backdrop: 'static', keyboard: false });
					$('#alert').on('shown.bs.modal', function () {

					})
				}if (result[0]=="ok") {
					$("#pembayaran").modal("hide");
					title = "Success";
					body = "<p>Data "+result[1]+" di simpan!</p>";				
				}
				$(".backModalUtama").on("click",function(){
					$("#pembayaran").modal("show");
				})
				pembayaranPage();
			}
		})
	});
})

$(document).on("click",".btn-hapus",function(){
	var id = $(this).attr("href");
	var nama = $(this).attr("data-nama");
	var obj = {}
	obj.id = id;
	obj.deleteBy = 1;
	var myJson = JSON.stringify(obj);
	var title = "Caution";
	var body = "anda akan menghapus "+nama+" ?";
	$(".modal-confirm-title").empty();
	$(".modal-confirm-title").append(title);
	$(".modal-confirm-body").empty();
	$(".modal-confirm-body").append(body);
	$(".modal-confirm").modal("show");
	$(".btn-confirm").on("click",function(){
	$.ajax({
		url : "http://localhost:8080/api/pembayaran/hapusDataKartu",
		type : "post",
		contentType : "application/json",
		data : myJson,
		success : function(result) {
			if (result == "ok") {
				$("#pembayaran").modal("hide");
				$(".modal-confirm").modal("hide");
				pembayaranPage();
			}	
		}	
	});
	})
})
	function pembayaranPage(){
		$.ajax({
			url : "http://localhost:8080/api/pembayaran/ShowAll",
			type : "GET",
			success : function(data) {
				var dompet ="<div class='container'>"+"<div class='mr-auto p-2 d-flex justify-content-between'>"+"<div><h4 clas='card-title'>Dompet Sehat</h4></div>";
				dompet += "<div><button class='btn btn-outline-primary btn-sm'  style='margin-right:5px;'>Aktifkan</button>"
				dompet += "<button class='btn btn-outline-primary'  style='margin-right:5px;'>Atur Pin</button>"
				dompet += "<button class='btn btn-outline-primary btn-sm'  style='margin-right:5px;'>Isi Saldo</button></div>"
				dompet += "</div>"	
					
				var kredit = "<div class='container'><div class='d-flex justify-content-between'>"+"<p class='card-title'>Kartu Kredit / Debit  ("+data.length+"/4 Tersimpan)</p>";
				kredit+="<button id='tambahKartu' class='btn btn-outline-primary'>Tambah</button>"+"</div>"
				kredit+="<table class='table table-borderless' style='border:0 !important'>"	
				for (var i = 0; i < data.length; i++) {
					kredit+="<thead><tr><th><p>"+data[i].cardNumber+"</p><p style='margin-top:-15px;'>"+String(data[i].validityPeriod).substring(2,7).replace('-','/')+"</p></th><th>Otto</th><th><i class='far fa-trash-alt' style='margin-right:5px;'></i><button href='"+data[i].id+"'class='btn btn-outline-warning btn-sm btn-hapus' data-nama='"+data[i].cardNumber+"'>Hapus</button></th></tr></thead>"
				}
				kredit+="</div></table>"
				kredit+="</div>"
				dompet+="</div>"
			    $(".body-credit-card").html(kredit);
				$(".dompet-credit-card").html(dompet);
			}
			
		})
	}

      
      
      
    

		
										    
										    
										    
										    