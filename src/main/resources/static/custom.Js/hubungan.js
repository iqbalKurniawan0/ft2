$(document)
		.ready(
				function() {
					hubunganPasien();
					function hubunganPasien() {
						$
								.ajax({
									url : "http://localhost:8080/api/hubunganPasien/hubungaPasienAll",
									type : "GET",
									success : function(data) {
										var str = "<table id='hubungan' class='table table-bordered table-striped'>";
										str += "<thead class='thead-dark'><tr><td>NAME</td><td>Action</td></tr></thead>";
										str += "<tbody>";
										for (var i = 0; i < data.length; i++) {
											str += "<tr><td>" + data[i].name
													+ "</td>";
											str += "<td><button href='"
													+ data[i].id + "," +data[i].name 
													+ "' class='editbtn btn btn-secondary'>EDIT </button> "
											str += "<button href='"
													+ data[i].id + "," +data[i].name 
													+ "' class='hpsbtn btn btn-warning'>HAPUS</button></td></tr>"
										}
										str += "</tbody>";
										str += "</table>";
										$(".card-body").empty();
										$(".card-body").append(str);
										$("#hubungan").DataTable({
											"paging" : false,
											"ordering" : false,
											"info" : false
										});
									}
								})

					}

					$("#tambahHubungan").click(function() {
						$.ajax({
							url : "/tambahDataHubunganPasien",
							type : "get",
							dataType : "html",
							success : function(result) {
								$("#tambah").modal("show");
								$("#isiHubunganPasien").html(result);
							}
						});

					})
					
					$(document).on('click','.hpsbtn',function() {
						var temp = $(this).attr("href");	
						var id = temp.split(',')[0];
						var nama = temp.split(',')[1];
						localStorage.setItem("nama", name);
						localStorage.setItem("deleteBy", id);
						$.ajax({
							url : "/deleteDataHubunganPasien",
							type : "get",
							dataType : "html",
							success : function(result) {
								$("#isiHubunganPasien").html(result);
								$(".spanname").append("<p>Anda akan menghapus " + nama +"</p>")
								$("#tambah").modal("show");
								
							}
						
						});
						return false;
					});
					
					$(document).on('click','.editbtn',function() {	
						var temp = $(this).attr("href");	
						var id = temp.split(',')[0];
						var nama = temp.split(',')[1];
						localStorage.setItem("nama", name);
						localStorage.setItem("updateId", id);
						$.ajax({
							url : "/editDataHubunganPasien",
							type : "get",
							dataType : "html",
							data: {
								nama : nama
							},
							success : function(result) {
								$("#isiHubunganPasien").html(result);
								$("#tambah").modal("show");
							}
						
						});
						return false;
					});
					

				})