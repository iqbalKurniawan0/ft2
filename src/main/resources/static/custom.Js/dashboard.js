$(document).ready(function(){
	$(".hubunganPasien").on("click",function(){
		$.ajax({
			url: "/hubungan",
			type : "get",
			dataType : "html",
			success : function(result){
				$("#fragment").html(result);
			}
		});
		return false;
	})
	
	$(".cariObat").on("click",function(){
		$.ajax({
			url: "/cariObat",
			type : "get",
			dataType : "html",
			success : function(result){
				$("#searchMed").html(result);
	
				$("#modalObat").modal("show");
			}
		})
	})
	
})

function tambahKeranjang(temp){
	$("button[href="+temp+"]").css("display","none");
	$("div[href="+temp+"]").show();
	$("input[href="+temp+"]").val(1);
}
	

function kurang(temp){
	var penjumlahan = $("input[href="+temp+"]").val();
	penjumlahan-=1;

	if (parseFloat(penjumlahan)<1) {
		$("button[href="+temp+"]").css("display","block");
		$("div[href="+temp+"]").css("display","none");
	}
	
	$("input[href="+temp+"]").val(penjumlahan);
	
	
}

function tambah(temp){
	var tambah = $("input[href="+temp+"]").val();
	tambah= parseFloat(tambah) + 1;
	
	console.log(tambah);
	
	$("input[href="+temp+"]").val(tambah);	
}


function clickProfile(){
	$.ajax({
		url: "/tabProfile",
		type : "get",
		dataType : "html",
		success : function(result){
			$("#fragment").html(result);
		}
	});
	return false;
}