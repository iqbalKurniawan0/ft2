$("#tambahDataPasEdit").click(function(e) {
		e.preventdefault;
		var obj = {};
		obj.name = $("#inputEditID").val();
		obj.nameOld = $("#inputEditHidden").val();
		obj.modifiedBy = 1;
		obj.id = parseFloat(localStorage.getItem("updateId"));		
		var myJSON = JSON.stringify(obj);
		if (obj.name == obj.nameOld) {
			var title = "Success";
			var str = "Data disimpan"
			$("#tambah").modal("hide");
			$(".modal-title-alert").empty();
			$(".modal-title-alert").append(title);
			$("#isialert").empty();
			$("#isialert").append(str);
			$("#alert").modal("show");	
			
			return false;
		}
		$.ajax({

			url : "http://localhost:8080/api/hubunganPasien/editPasien",
			type : "POST",
			contentType : "application/json",
			data : myJSON,
			success : function(data) {
				var title = "";
				var str = "";
				if (data == "ok") {
					title += "Success";
					str+="<p style='color:green;'>Data berhasil ubah!</p>";
					hubunganPasien();
				}else if (data=="kosong") {
					title += "Failed"
						str+="<p style='color:red;'>Nama tidak boleh kosong!</p>"
				}else if(data == "ada"){
					title += "Failed";
					str+="<p style='color:red;'>Nama sudah ada!</p>";
				}
				$("#tambah").modal("hide");
				$(".modal-title-alert").empty();
				$(".modal-title-alert").append(title);
				$("#isialert").empty();
				$("#isialert").append(str);
				$("#alert").modal("show");	
			}
		})
	})


function hubunganPasien() {
						$
								.ajax({
									url : "http://localhost:8080/api/hubunganPasien/hubungaPasienAll",
									type : "GET",
									success : function(data) {
										var str = "<table id='hubungan' class='table table-bordered table-striped'>";
										str += "<thead class='thead-dark'><tr><td>NAME</td><td>Action</td></tr></thead>";
										str += "<tbody>";
										for (var i = 0; i < data.length; i++) {
											str += "<tr><td>" + data[i].name
													+ "</td>";
											str += "<td><button href='"
													+ data[i].id + "," + data[i].name
													+ "' class='editbtn btn btn-secondary'>EDIT </button> "
											str += "<button href='"
													+ data[i].id
													+ "' class='hpsbtn btn btn-warning'>HAPUS</button></td></tr>"
										}
										str += "</tbody>";
										str += "</table>";
										$(".card-body").empty();
										$(".card-body").html(str);
										$("#hubungan").DataTable({
											"paging" : false,
											"ordering" : false,
											"info" : false
										});
									}
								})

					}
	
$(document).on('click','.editbtn',function() {	
	var temp = $(this).attr("href");	
	var id = temp.split(',')[0];
	var nama = temp.split(',')[1];
	localStorage.setItem("nama", name);
	localStorage.setItem("updateId", id);
	$.ajax({
		url : "/editDataHubunganPasien",
		type : "get",
		dataType : "html",
		data : {
			nama : nama
		},
		success : function(result) {
			$("#isiHubunganPasien").html(result);
			$("#tambah").modal("show");
		}
	
	});
	return false;
});