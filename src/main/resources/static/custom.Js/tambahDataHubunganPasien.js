 $(document).ready(function(){
	$("#tambahDataPas").click(function(){
		var obj = {};
		obj.name = $("#exampleNama").val();
		obj.createdBy = 1;
		var myJson = JSON.stringify(obj);
		
		$.ajax({
			url			:"http://localhost:8080/api/hubunganPasien/insertDataHubunganPasien",
			type		:"POST",
			contentType	:"application/json",
//			dataType	: "json", 
			data		: myJson,
			success	: function(data){
				var title = "";
				var str = "";
				if (data == "ok") {
					title += "Success"
					str+="<h5 style='color:green;'>Data berhasil ditambah!</h5>";
				}else if (data=="kosong") {
					title += "Failed"
					str+="<h5 style='color:red;'>Nama tidak boleh kosong!</h5>";
				}else if(data == "ada"){
					title += "Failed";
					str+="<h5 style='color:red;'>Nama sudah ada!</h5>";
				}
				hubunganPasien();
				$("#tambah").modal("hide");
				$(".modal-title-alert").empty();
				$(".modal-title-alert").append(title);
				$("#isialert").empty();
				$("#isialert").append(str);
				$("#alert").modal("show");
				hubunganPasien();
			
			
			}
		});
		
	})

})

function hubunganPasien() {
						$
								.ajax({
									url : "http://localhost:8080/api/hubunganPasien/hubungaPasienAll",
									type : "GET",
									success : function(data) {
										var str = "<table id='hubungan' class='table table-bordered table-striped'>";
										str += "<thead class=''><tr><td>NAME</td><td>Action</td></tr></thead>";
										str += "<tbody>";
										for (var i = 0; i < data.length; i++) {
											str += "<tr><td>" + data[i].name
													+ "</td>";
											str += "<td><button href='"
													+ data[i].id + "," +data[i].name 
													+ "' class='editbtn btn btn-secondary' style='margin-right:30px;'> EDIT </button> "
											str += "<button href='"
												+ data[i].id + "," +data[i].name 
												+ "' class='hpsbtn btn btn-warning'>HAPUS</button></td></tr>"
										}
										str += "</tbody>";
										str += "</table>";
										$(".card-body").empty();
										$(".card-body").append(str);
										$("#hubungan").DataTable({
											"paging" : false,
											"ordering" : false,
											"info" : false
										});
									}
								})

					}