insert into m_medical_item_category(name) values('Demam')
insert into m_medical_item_category(name) values('Batuk')
insert into m_medical_item_category(name) values('Supplement')
insert into m_medical_item_category(name) values('Jantung')

insert into m_medical_item(name,composition,medical_item_catagory_id,medical_item_segmentation_id,image_path) values('panadol Extra','Panadol merah bermanfaat untuk meredakan sakit kepala dan sakit gigi. Tiap tablet Panadol Extra mengandung 500 mg paracetamol dan 65 mg kafein',1,1,'https://i-cf5.gskstatic.com/content/dam/cf-consumer-healthcare/panadol/id_id/product-images/Panadol%20Caplet_455x455.png?auto=format')
insert into m_medical_item(name,composition,medical_item_catagory_id,medical_item_segmentation_id) values('panadol','Panadol biru bermanfaat untuk meredakan nyeri, seperti sakit kepala, sakit gigi, dan nyeri otot, serta menurunkan demam. Tiap tablet Panadol Reguler mengandung 500 mg paracetamol',1,1)
insert into m_medical_item(name,composition,medical_item_catagory_id,medical_item_segmentation_id) values('panadol Flu dan Batuk','Panadol Flu & Batuk bermanfaat untuk meredakan bersin-bersin, hidung tersumbat, demam, sakit kepala, batuk tidak berdahak, nyeri otot, dan nyeri tenggorokan akibat flu',1,1)
insert into m_medical_item(name,composition,medical_item_catagory_id,medical_item_segmentation_id) values('panadol Anak','Panadol Anak Drops untuk anak-anak usia 0–1 tahun tersedia dalam bentuk sirop yang dilengkapi dengan pipet tetes. Tiap 1 ml Panadol Anak Drops mengandung 100 mg paracetamol',1,1)
insert into m_medical_item(name,composition,medical_item_catagory_id,medical_item_segmentation_id) values('Bodrex','Bodrex bermanfaat untuk mengatasi sakit kepala, sakit gigi, dan demam. Produk ini tersedia dalam bentuk tablet. Kandungan tiap tablet Bodrex adalah 500 mg paracetamol dan 50 mg kafein.',1,1)
insert into m_medical_item(name,composition,medical_item_catagory_id,medical_item_segmentation_id) values('panadol Extreme','Obat keras harus dengan resep dokter',1,2)
insert into m_medical_item(name,composition,medical_item_catagory_id,medical_item_segmentation_id) values('Viks Formula 44 sirup','Vicks Formula 44 Sirup 100 ML mengandung kombinasi zat aktif Dextromethorphan dan Doxylamine Succinate. Obat ini digunakan untuk mengatasi batuk berdahak, batuk kering, infeksi saluran napas, maupun peradangan pada hidung',2,1)
insert into m_medical_item(name,composition,medical_item_catagory_id,medical_item_segmentation_id) values('YOU-C 1000','YOU-C 1000 Orange adalah minuman kesehatan dengan 1000 mg Vitamin C yang membantu penerapan gaya hidup sehat dan membuat Anda merasa segar setiap harinya. Minuman ini mengandung 1000 mg Vitamin C untuk membantu meningkatkan sistem daya tahan tubuh',3,1)
insert into m_medical_item(name,composition,medical_item_catagory_id,medical_item_segmentation_id) values('Simvastatin','SIMVASTATIN merupakan senyawa antilipermic derivat asam mevinat golongan statin, yang digunakan sebagai obat penurun kolesterol. Obat ini bekerja sebagai inhibitor kompetitif pada HMG-CoA reduktase',4,2)

insert into m_medical_item_segmentation(name) values('obat resep')
insert into m_medical_item_segmentation(name) values('obat tanpa resep')

insert into t_customer_registered_card(customer_id,card_number,validity_period,cvv) values(1,'6392-5642-9800-5243','2023-08-21','321')
insert into t_customer_registered_card(customer_id,card_number,validity_period,cvv) values(1,'7834-8901-0021-4425','2022-01-18','123')
insert into t_customer_registered_card(customer_id,card_number,validity_period,cvv) values(1,'6392-5642-9800-1212','2021-08-21','321')
insert into t_customer_registered_card(customer_id,card_number,validity_period,cvv) values(1,'7834-8901-0021-1313','2025-01-18','123')
	

insert into m_costumer_relation(name) values('ibu')
insert into m_costumer_relation(name) values('ayah')
insert into m_costumer_relation(name) values('adik')