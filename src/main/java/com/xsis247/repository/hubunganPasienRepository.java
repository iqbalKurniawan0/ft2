package com.xsis247.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.xsis247.model.hubunganPasien;

public interface hubunganPasienRepository extends JpaRepository<hubunganPasien, Long> {
	//menampilkan semua
	@Query(value="select * from m_costumer_relation where is_Delete = false order by id", nativeQuery = true)
	List<hubunganPasien>hubunganPasienAll();
	//cari berdasarkan id
	@Query(value = "select * from m_costumer_relation where id=?1", nativeQuery = true)
	List<hubunganPasien>PasienId(long id);
	//cari berdasrkan nama
	@Query(value="select name from m_costumer_relation where id=?1 limit 1", nativeQuery = true)
	String cekNama(long id);
	@Query(value="select count(1) from m_costumer_relation where name=?1 limit 1",nativeQuery = true)
	long cariNama2(String name);
	//update/edit
	@Modifying
	@Query(value="update m_costumer_relation set name=?1, modified_by=?2, modifie_on=now() where id=?3 ", nativeQuery = true)
	void editPasien(@Param("name")String name, @Param("modifiedBy")Long modifiedBy, @Param("id") Long id);
	//delete beneran
	@Modifying
	@Query(value="delete from m_costumer_relation where id=?1",nativeQuery = true)
	void deletePasien(@Param("id")Long id, @Param("deleteBy")Long deleteBy);	
	//delete boolean
	@Modifying
	@Query(value="update m_costumer_relation set delete_by=?2, delete_on=now(), is_delete=true where id=?1", nativeQuery = true)
	void deletepasien2(@Param("id")Long id, @Param("deleteBy") Long delete_by);
	//insert
	@Modifying
	@Query(value="insert into m_costumer_relation (name,created_By,created_On) values(?1,?2,now())",nativeQuery = true)
	void insertDataHubunganPasien(@Param("name") String name, @Param("createdBy")Long createdBy);
} 
	