package com.xsis247.repository;

import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.xsis247.model.medicalItemCatagory;

public interface cariObatRepository extends JpaRepository<medicalItemCatagory, Long> {
	@Query(value="select * from m_medical_item_category order by id", nativeQuery = true)
	List<medicalItemCatagory>selectAllKatagori();
	
	@Query(value="select MC.name as kategori,MI.name As katakunci, MIS.name as Segmentasi, MI.composition as komposisi, "
			+ "MI.image_path as gambar, MI.id from m_medical_item MI " + 
			" join m_medical_item_category MC on MI.medical_item_catagory_id = MC.id  join " + 
			" m_medical_item_segmentation MIS on MI.medical_item_segmentation_id = MIS.id " +
			" where (COALESCE(?1,'')='' or lower(MC.name)=?1) and (COALESCE(?2,'') = '' or lower(MI.name) like %?2%)"
			+ " and  MIS.id=?3 ", nativeQuery = true)
	List<Map<String, Object>>hasilCari(String kategori,String katakunci,Long segmnt);
	
}
 	