package com.xsis247.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.xsis247.model.pembayaran;

public interface pembayaranRepository extends JpaRepository<pembayaran, Long> {
	//Menampilkan Semua
	@Query(value="select * from t_customer_registered_card where is_Delete = false and customer_id =?1 order by id ",nativeQuery = true)
	List<pembayaran>ShwoAll(long customerId);
	
	//Cari no Kartu
	@Query(value="select card_number from t_customer_registered_card where card_Number=?1 and is_delete=false limit 1",nativeQuery = true)
	String cariNoKartu(String cardNumber);
	//cekSlot
	@Query(value="select count(1) from t_customer_registered_card where is_delete=false and customer_id =?1",nativeQuery = true)
	long cekSlot(Long cekSlot);
	//insert
	@Modifying
	@Query(value="insert into t_customer_registered_card (card_number,validity_period,cvv,customer_id,created_by,created_on) "
			+ "values(?1,?2,?3,?4,?4,now())",nativeQuery = true)
	void tambahDaftarKartu(@Param("cardNumber") String cardNumber, @Param("validityPeriod")String validityPeriod,@Param("tempCVV")String tempCVV, @Param("createdBy")Long createdBy);
	
	//delete boolean
	@Modifying
	@Query(value="update t_customer_registered_card set delete_by=?1, delete_on=now(), is_delete=true where id=?2",nativeQuery = true)
	void hapusBoolean(long deleteBy, long id); 
	
}
