package com.xsis247.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xsis247.model.dtoHasilCari;
import com.xsis247.model.medicalItem;
import com.xsis247.model.medicalItemCatagory;
import com.xsis247.model.medicalItemSegmentation;
import com.xsis247.service.cariObatService;

@RestController
@RequestMapping("api/cariObat")
public class cariObatControllerRest {
	
	@Autowired
	private cariObatService cos;
	private medicalItem mi;
	private medicalItemCatagory mc;
	private medicalItemSegmentation ms;
	
	@GetMapping("/selectAllKatagori")
	public List<medicalItemCatagory>selectAllKatagori(){
		List<medicalItemCatagory>selectAllKatagori = cos.selectAllKatagori();
		return selectAllKatagori;
	}
	
	@GetMapping("/hasilCari/{param}")
	public List<Map<String, Object>>hasilCari(@PathVariable String[]param){	

		List<Map<String, Object>>hasiCari = cos.hasilCari(param[0].toLowerCase(),param[1].toLowerCase(),Long.parseLong(param[2]));
		return hasiCari;
	}
}
