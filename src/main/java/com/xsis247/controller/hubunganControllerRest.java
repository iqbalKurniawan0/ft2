package com.xsis247.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xsis247.model.hubunganPasien;
import com.xsis247.service.hubunganPasienService;

import net.bytebuddy.asm.Advice.Return;

@RestController
@RequestMapping("api/hubunganPasien")
public class hubunganControllerRest {

	@Autowired
	private hubunganPasienService hps;

	@GetMapping("/hubungaPasienAll")
	public List<hubunganPasien> hubunganPasienAll() {
		List<hubunganPasien> hubunganPasienAll = hps.hubunganPasienAll();
		return hubunganPasienAll;

	}
	
	@GetMapping("/PasienId/{id}")
	public List<hubunganPasien>PasienId(@PathVariable long id){
		List<hubunganPasien>PasienId = hps.PasienId(id);
		return PasienId;
	}

	@PostMapping("/editPasien")
	public String editPasien(@RequestBody hubunganPasien hp) {
		long cariNama = hps.cariNama2(hp.getName());
		String cekNama = hps.cekNama(hp.getId());
				
		System.out.print(hp.getName() != hp.getNameOld());
		if (hp.getName()=="") {
			return "kosong";
		}if (hp.getName() == hp.getNameOld()) {
			return "ok";
		}if (hp.getName() != hp.getNameOld()) {
			if (cariNama != 0 && cekNama != null ) {
				return "ada";
			}else {
				hps.editPasien(hp.getName(), hp.getModifiedBy(), hp.getId());
				return "ok";
			}
		}else {
			return "error";
		}
			
		
	}
	
	@PostMapping(value="/deletePasien2")
	public String deletePasien2(@RequestBody hubunganPasien hp) {
		String status = "";
		hps.deletePasien2(hp.getId(), hp.getDeleteBy());;
		status = "ok";
		
		return status; 
		
	}
	
	@PostMapping(value="/insertDataHubunganPasien")
	public String insertDataHubunganPasien(@RequestBody hubunganPasien hp) {
		String status = "";
		long cariNama = hps.cariNama2(hp.getName());
		if (hp.getName()=="") {
			status = "kosong";
		}
		else if(cariNama == 0) {
			hps.insertDataHubunganPasien(hp.getName(), hp.getCreatedBy());
			status = "ok";
		}else {
			status = "ada";
		}	
		
		return status; 
	}
	
	
}
