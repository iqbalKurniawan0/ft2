package com.xsis247.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
@Controller
public class cariObatController {

	@RequestMapping("/cariObat")
	public String cariObat() {
		return "/cariObat/cariObatHTML";
	}
	
	@RequestMapping("/hasilcari")
	public String hasilcari() {
		return "/cariObat/HasilCariObat";
	}
	
	
}
