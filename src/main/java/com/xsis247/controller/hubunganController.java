package com.xsis247.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
@Controller	 
public class hubunganController {
	
	@RequestMapping("/hubungan")
	public String hubungan() {
		return "/Hubungan/hubungan";
	}
	
	@RequestMapping("tambahDataHubunganPasien")
	public String tambahDataHubunganPasien() {
		return "/Hubungan/tambahDataHubunganPasien";
	}
	
	@RequestMapping("editDataHubunganPasien")
	public String editDataHubunganPasien(@RequestParam(value = "nama",required=false)String nama, Model model) {	
		model.addAttribute("nama",nama);
		return "/Hubungan/editHubungan";
	}
	
	@RequestMapping("deleteDataHubunganPasien")
	public String deleteDataHubunganPasien() {
		return "/Hubungan/deleteDataHubunganPasien";
	}
}
