package com.xsis247.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xsis247.model.pembayaran;
import com.xsis247.service.pembayaranService;

@RestController
@RequestMapping("api/pembayaran")
public class pembayaranControllerRest {
	@Autowired
	private pembayaranService ps;
	
	@GetMapping("/ShowAll")
	public List<pembayaran>ShowAll(){
		long customer_id = 1;
		List<pembayaran>ShowAll = ps.ShowAll(customer_id);
		return ShowAll;
	}
	
	@PostMapping("/tambahDaftarKartu")
	public String tambahDaftarKartu(@RequestBody pembayaran p) {
		//cek inputan bersal dari depan 
		if (p.getCardNumber()=="") {
			return "kosong,number";
		}if (p.getTempCVV() == "") {
			return "kosong,cvv";
		}if(p.getValidityPeriod()=="") {
			return "kosong,validasi";
		}
		else {
			long cekSlot = ps.CekSlot(p.getCreatedBy());
			String carikartu = ps.cariNoKartu(p.getCardNumber());
			if (cekSlot >= 4) {
				return "full,slot";
			}
			if(carikartu == null) {
				ps.tambahDaftarKartu(p.getCardNumber(), p.getValidityPeriod(), p.getTempCVV(), p.getCreatedBy());
				return "ok,"+p.getCardNumber();
			}else {
				return "ada,"+p.getCardNumber();
			}
		}
	}
	
	@PostMapping("/hapusDataKartu")
	public String hapusBoolean(@RequestBody pembayaran p) {
		ps.hapusBoolean(p.getDeleteBy(), p.getId());
		return "ok";
	}
}
