package com.xsis247.model;


import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="m_medical_item")
public class medicalItem {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	
	@Column(name="id")
	private long id;
	
	@Column(name="name")
	private String name;
	
	@Column(name="medicalItemCatagoryId")
	private long medicalItemCatagoryId;
	
	@Column(name="composition")
	private String composition;
	
	@Column(name="medicalItemSegmentationId")
	private long medicalItemSegmentationId;
	
	@Column(name="image")
	private byte[] image;
	
	@Column(name="imagePath")
	private String imagePath;
	
	@Column(name="createBy")
	private long createBy;
	
	@Column(name="createOn")
	private Timestamp createOn;

	@Column(name="modifiedBy")
	private long modifiedBy;
	
	@Column(name="modifiedOn")
	private Timestamp modifiedOn;
	
	@Column(name="deleteBy")
	private long deleteBy;
	
	@Column(name="deleteOn")
	private Timestamp deleteOn;
	
	@Column(name="idDelete")
	private boolean idDelete;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getMedicalItemCatagoryId() {
		return medicalItemCatagoryId;
	}

	public void setMedicalItemCatagoryId(long medicalItemCatagoryId) {
		this.medicalItemCatagoryId = medicalItemCatagoryId;
	}

	public String getComposition() {
		return composition;
	}

	public void setComposition(String composition) {
		this.composition = composition;
	}

	public long getMedicalItemSegmentationId() {
		return medicalItemSegmentationId;
	}

	public void setMedicalItemSegmentationId(long medicalItemSegmentationId) {
		this.medicalItemSegmentationId = medicalItemSegmentationId;
	}

	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public long getCreateBy() {
		return createBy;
	}

	public void setCreateBy(long createBy) {
		this.createBy = createBy;
	}

	public Timestamp getCreateOn() {
		return createOn;
	}

	public void setCreateOn(Timestamp createOn) {
		this.createOn = createOn;
	}

	public long getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Timestamp getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Timestamp modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public long getDeleteBy() {
		return deleteBy;
	}

	public void setDeleteBy(long deleteBy) {
		this.deleteBy = deleteBy;
	}

	public Timestamp getDeleteOn() {
		return deleteOn;
	}

	public void setDeleteOn(Timestamp deleteOn) {
		this.deleteOn = deleteOn;
	}

	public boolean isIdDelete() {
		return idDelete;
	}

	public void setIdDelete(boolean idDelete) {
		this.idDelete = idDelete;
	}
	
	
}




	


