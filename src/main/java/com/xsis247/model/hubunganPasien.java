package com.xsis247.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="m_costumer_relation")
public class hubunganPasien {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private long id;
	
	
	@Column(name="name")
	private String name;


	@Column(name="createdBy")
	private Long createdBy;
	
	@Column(name="createdOn")
	private Timestamp createdOn;
	
	@Column(name="modifiedBy")
	private Long modifiedBy;
	
	@Column(name="modifieOn")
	private Timestamp modifieOn;
	
	@Column(name="deleteBy")
	private Long deleteBy;
	
	@Column(name="deleteOn")
	private Timestamp deleteOn;
	
	private String nameOld;
	
	@Column(name="isDelete", columnDefinition = "boolean default false")
	private Boolean isDelete = false;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	public Long getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Timestamp getModifieOn() {
		return modifieOn;
	}

	public void setModifieOn(Timestamp modifieOn) {
		this.modifieOn = modifieOn;
	}

	public Long getDeleteBy() {
		return deleteBy;
	}

	public void setDeleteBy(Long deleteBy) {
		this.deleteBy = deleteBy;
	}

	public Timestamp getDeleteOn() {
		return deleteOn;
	}

	public void setDeleteOn(Timestamp deleteOn) {
		this.deleteOn = deleteOn;
	}

	public String getNameOld() {
		return nameOld;
	}

	public void setNameOld(String nameOld) {
		this.nameOld = nameOld;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

	
}
