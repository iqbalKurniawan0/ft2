package com.xsis247.model;

public class dtoHasilCari {
	private String kategori;
	private String katakunci;
	public dtoHasilCari(String kategori, String katakunci) {
		this.kategori = kategori;
		this.katakunci = katakunci;
	}
	public String getKategori() {
		return kategori;
	}
	public void setKategori(String kategori) {
		this.kategori = kategori;
	}
	public String getKatakunci() {
		return katakunci;
	}
	public void setKatakunci(String katakunci) {
		this.katakunci = katakunci;
	}
	
	

	
}
