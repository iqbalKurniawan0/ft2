package com.xsis247.model;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="t_customer_registered_card")
public class pembayaran {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private long id;
	
	@Column(name="customerId")
	private long customerId;
	
	@Column(name="cardNumber ")
	private String cardNumber;
	
	@Column(name="validityPeriod")
	private String validityPeriod;
	
	@Column(name="cvv")
	private String tempCVV;
	
	@Column(name="createdBy")
	private Long createdBy;
	
	@Column(name="createdOn")
	private Timestamp createdOn;
	
	@Column(name="modifiedBy")
	private Long modifiedBy;
	
	@Column(name="modifieOn")
	private Timestamp modifieOn;
	
	@Column(name="deleteBy")
	private Long deleteBy;
	
	@Column(name="deleteOn")
	private Timestamp deleteOn;
	
	@Column(name="isDelete", columnDefinition = "boolean default false")
	private Boolean isDelete = false;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getValidityPeriod() {
		return validityPeriod;
	}

	public void setValidityPeriod(String validityPeriod) {
		this.validityPeriod = validityPeriod;
	}

	public String getTempCVV() {
		return tempCVV;
	}

	public void setTempCVV(String tempCVV) {
		this.tempCVV = tempCVV;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	public Long getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Timestamp getModifieOn() {
		return modifieOn;
	}

	public void setModifieOn(Timestamp modifieOn) {
		this.modifieOn = modifieOn;
	}

	public Long getDeleteBy() {
		return deleteBy;
	}

	public void setDeleteBy(Long deleteBy) {
		this.deleteBy = deleteBy;
	}

	public Timestamp getDeleteOn() {
		return deleteOn;
	}

	public void setDeleteOn(Timestamp deleteOn) {
		this.deleteOn = deleteOn;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}
	
}
