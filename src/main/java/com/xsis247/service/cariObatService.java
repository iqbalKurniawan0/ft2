package com.xsis247.service;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.xsis247.model.medicalItemCatagory;
import com.xsis247.repository.cariObatRepository;

@Service
@Transactional
public class cariObatService {
	@Autowired
	private cariObatRepository cor;
	
	
	public List<medicalItemCatagory>selectAllKatagori(){
		return cor.selectAllKatagori();
	}
	
	public List<Map<String, Object>>hasilCari(String kategori,String katakunci,Long segmnt){
		return cor.hasilCari(kategori, katakunci, segmnt);
	}
	
}
