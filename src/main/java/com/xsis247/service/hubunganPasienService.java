package com.xsis247.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xsis247.model.hubunganPasien;
import com.xsis247.repository.hubunganPasienRepository;

@Service
@Transactional
public class hubunganPasienService {
	@Autowired
	hubunganPasienRepository hps;
	
	public List<hubunganPasien>hubunganPasienAll(){
		return hps.hubunganPasienAll();
	}
	
	
	public List<hubunganPasien>PasienId(long id){
		return hps.PasienId(id);
	}
	
	public void editPasien(String name, Long modifiedBy, Long id) {
		hps.editPasien(name,modifiedBy, id);
	}
	
	public void deletePasien2(long id, long deleteBy) {
		hps.deletepasien2(id, deleteBy);
	}
	
	public void insertDataHubunganPasien(String name, long createdBy) {
		hps.insertDataHubunganPasien(name, createdBy);
	}
	
	public long cariNama2 (String name) {
		return hps.cariNama2(name);
	}
	
	public String cekNama(long id) {
		return hps.cekNama(id);
	}
}
