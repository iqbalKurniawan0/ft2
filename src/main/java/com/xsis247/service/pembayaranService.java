package com.xsis247.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xsis247.model.pembayaran;
import com.xsis247.repository.pembayaranRepository;

@Service
@Transactional
public class pembayaranService {
	@Autowired
	pembayaranRepository pr;
	
	public List<pembayaran>ShowAll(long customer_id){
		return pr.ShwoAll(customer_id);
	}
	
	public String cariNoKartu (String cardNumber) {
		return pr.cariNoKartu(cardNumber);
	}
	
	public long CekSlot(long cekSlot) {
		return pr.cekSlot(cekSlot);
	}
	public void tambahDaftarKartu(String cardNumber, String validityPeriod, String tempCVV, Long createdBy) {
		pr.tambahDaftarKartu(cardNumber, validityPeriod, tempCVV, createdBy);
	}
	
	public void hapusBoolean(long deleteBy, long id) {
		pr.hapusBoolean(deleteBy, id);
	}
}
