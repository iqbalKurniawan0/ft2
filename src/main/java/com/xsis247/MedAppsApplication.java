package com.xsis247;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MedAppsApplication {

	public static void main(String[] args) {
		SpringApplication.run(MedAppsApplication.class, args);
	}

}
